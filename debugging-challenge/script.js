const registerForm = document.querySelector("#registerForm")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault();

	const username = document.querySelector("#username")
	const email = document.querySelector("#email")
	const mobileNo = document.querySelector("#mobileNo")
	const password1 = document.querySelector("#password1")
	const password2 = document.querySelector("#password2")

	let condition1 = false;
	let condition2 = false;
	let condition3 = false;

	if(password1.value.length < 6){
		alert("PASSWORD MUST BE AT LEAST 6 CHARACTERS LONG")
	}else{
		condition1 = true
	};

	if(password1.value.search(/[A-Z]/) < 0) {
	    alert("PASSWORD MUST INCLUDE AT LEAST ONE UPPERCASE CHARACTER")
	}else{
		condition2 = true
	}

	if(password1.value !== password2.value){
		alert("PASSWORDS MUST MATCH")
	}else{
		condition3 = true
	}

	if(condition1 === true && condition2 === true && condition3 === true){
		alert(`USER ${username.value} REGISTERED WITH EMAIL ${email.value} AND MOBILE NUMBER ${mobileNo.value}`)

		username.value = ""
		email.value =  ""
		mobileNo.value = ""
		password1.value = ""
		password2.value = ""
	}
})